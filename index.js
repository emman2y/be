const feather = require('@feathersjs/feathers')
const express = require('@feathersjs/express')
const bodyParser = require('body-parser')
const configuration = require('@feathersjs/configuration')
const cors = require('cors')

const app = express(feather())
  .configure(configuration())
  .configure(express.rest())

const response = [
  {
    country: 'Philippines',
    province: 'Cebu',
    place: 'Cebu City',
    condition: 'Cloundy with the chance of rain and thunderstorm',
    temperature: '21',
    unit: 'Celcius',
    zipCode: '6000'
  },
  {
    country: 'Philippines',
    province: 'Cebu',
    place: 'Mandaue City',
    condition: 'Sunny with 10% chance of rain',
    temperature: '25',
    unit: 'Celcius',
    zipCode: '6015'
  },
  {
    country: 'Philippines',
    province: 'Cebu',
    place: 'Talisay City',
    condition: 'Cloudy',
    temperature: '22',
    unit: 'Celcius',
    zipCode: '6002'
  },
  {
    country: 'Philippines',
    province: 'Cebu',
    place: 'Minglanilla City',
    condition: 'Rain and scattered thunderstorm',
    temperature: '21',
    unit: 'Celcius',
    zipCode: '6003'
  },
  {
    country: 'Philippines',
    province: 'Cebu',
    place: 'Liloan City',
    condition: 'Slighly chance of rain',
    temperature: '24',
    unit: 'Celcius',
    zipCode: '6020'
  },
  {
    country: 'Philippines',
    province: 'Cebu',
    place: 'Carmen city',
    condition: 'Rain and Thunderstorm',
    temperature: '21',
    unit: 'Celcius',
    zipCode: '6010'
  },
]

app.use(cors())

const server = require('http').createServer(app)
const io = require('socket.io')(server)

app.use(bodyParser.json({ type: 'application/**json' })) 
app.use(bodyParser.json())

app.use('/api', {
  find() {
    return Promise.resolve(response)
  },
  create(data) {
    const fd = data.zipcode
    
    const result = response.filter(function(d){
      if (!!fd) {
        return d.zipCode == fd
      } else {
        return d
      }
    })

    return Promise.resolve(result)
  }

})

server.listen(8383, function() {
  console.log('listend on 8383')
})

